

# Mutmut


## Requirements

* php7.4 (or more?)
* php-mbstring
* php-sqlite3
* php-twig


## Install

1. Git clone this repo, or download zip and extract
2. Copy the whole folder to your server
3. Add a virtual host pointing to the `htdocs` subfolder (see below)
4. Navigate to your installation e.g. http://localhost:8080
5. On first visit, an 'admin' user has been created with password 'admin'
6. Log in with admin/admin, change your password and start adding users


## Virtual host

Your virtual host should point to (have its document root on) the `htdocs` subfolder.

This prevents unwanted access to the database or other files, that are outside `htdocs`.

Here is an example *vhost* configuration when using the *apache* web server:
(replace `mutmut.site` with the URL to access your site)

```
# redirection http => https
<VirtualHost *:80>
  ServerName mutmut.site
  Redirect permanent / https://mutmut.site/
</VirtualHost>

<IfModule mod_ssl.c>
  <VirtualHost *:443>
  ServerName mutmut.site

  DocumentRoot /path/to/mutmut/htdocs
  <Directory /path/to/mutmut/htdocs>
    Require all granted
  </Directory>

  ErrorLog /var/log/apache2/mutmut_error.log
  CustomLog /var/log/apache2/mutmut_access.log combined

  </VirtualHost>
</IfModule>
```


## Testing the install & first visit

Visit <mutmut.site>.

You should see the login page.
You can log in with user admin (password: admin) and start adding users.

If your browser tells you it *can't access the page*, check your virtual host configuration.
Also make sure you have reloaded your web server, and check its logs...

If you see a *blank page* (code 500-something), it probably means the database couldn't be initialised.
If so, modifying the ownership of the database and parent folder will usually suffice:

```
ssh my.server
cd /path/to/mutmut               # not htdocs
chown -R www-data:www-data data  # or whichever your webserver user is
```

Also allow twig to write its cache files in the `twig directory`:

```
mkdir twig/cache
chown -R www-data:www-data twig/cache  # or whichever your webserver user is
```

If you keep getting a blank page, check your php logs for more info.

