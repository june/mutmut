<?php

require_once '../lib/auth_check.php';
require_once '../lib/money.php';
require_once '../lib/twig.php';


@session_start();

if (!Money::ContributionEditable($_SESSION['user']['name']))
{
	header('Location: /');
	die();
}

$calc_contrib = [];
if (isset($_POST['income']))
{
	$_POST['income'] = (int)$_POST['income'];
	$_POST['rent']   = (int)$_POST['rent'];
	$contrib = round(($_POST['income'] - $_POST['rent']) * 0.1);
	$calc_contrib = ['user_contrib'=>['edit_contrib'=>['amount'=>$contrib]]];
}

$contribution = $_POST['contribution'] ?? null;
if (!is_null($contribution))
{
	if ($contribution !== '0' && (float)$contribution == 0)  // contribution must be an integer; 0 is also valid
		die('Revenu incorrect');
	
	Money::DeclareContribution($_SESSION['user']['name'], (float)$contribution);
	
	header('Location: /contributions.php');
	die();
}


Twig::Display('declare.html', ['contributions','post'], $calc_contrib);
