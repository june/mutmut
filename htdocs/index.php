<?php

require_once '../lib/auth_check.php';
require_once '../lib/twig.php';

Twig::Display('index.html', ['registers','transactions','contributions']);
