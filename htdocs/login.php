<?php

require_once '../lib/common.php';
require_once '../lib/twig.php';
require_once '../lib/users.php';


@session_start();

// log out
if (isset($_GET['out']))
{
	unset($_SESSION);
	session_destroy();
	header('Location: /login.php');
	die();
}


// logged in, nothing to do here: redirect to '/'
if ($_SESSION['user']['name'] ?? false)
{
	header('Location: /');
	die();
}


// try to login
$login    = $_POST['login']    ?? null;
$password = $_POST['password'] ?? null;

if ($login || $password)
{
	$user = Users::Login($login,$password);
	if ($user)
	{
		$_SESSION['user'] = $user;
		header('Location: '.($_POST['redirect'] ?: '/'));
		die();
	}
	die("Erreur d'authentification");
}


Twig::Display('login.html');
