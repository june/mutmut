<?php

require_once '../lib/auth_check.php';
require_once '../lib/twig.php';

Money::HandlePaymentAction('/needs.php');
Money::HandleNeedAction('/needs.php');

Twig::Display('needs.html', ['needs', 'registers', 'contributions', 'payments_redistribution']);
