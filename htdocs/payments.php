<?php

require_once '../lib/auth_check.php';
require_once '../lib/twig.php';

Money::HandlePaymentAction('/payments.php');

Twig::Display('payments.html', ['payments_mine','payments_others']);
