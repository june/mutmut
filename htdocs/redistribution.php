<?php

require_once '../lib/auth_check.php';
require_once '../lib/twig.php';

Money::HandlePaymentAction('/redistribution.php');

Twig::Display('redistribution.html', ['equilibrium','payments_redistribution','contributions','needs']);
