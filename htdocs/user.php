<?php

require_once '../lib/auth_check.php';
require_once '../lib/twig.php';


$action = $_POST['action'] ?? null;
switch ($action)
{
	case 'create':
		Users::Create($_POST['name'], $_POST['pronoun'], $_POST['password']);
		header('Location: /index.php');
		die();
		break;
	
	case 'edit':
		Users::Edit($_POST['name'], $_POST['pronoun'], $_POST['password']);
		header('Location: /user.php');
		die();
		break;
	
	case 'give_accountant_role':
	case 'leave_accountant_role':
		try
		{
			Users::GiveAccountantRole($_POST['user_new_accountant'] ?? null);
		}
		catch (\Exception $e)
		{
			die("Impossible de passer le rôle à la personne sélectionnée (déjà comptable ?)");
		}
		header('Location: /user.php');
		die();
		break;
}

@session_start();

$action = $_GET['action'] ?? null;
if ($action == 'create' && $_SESSION['user']['role'] != 'accountant')
{
	header("Location: /user.php");
	die();
}

Twig::Display('user.html', ['get', 'payments_redistribution']);
