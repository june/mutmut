<?php

require_once 'common.php';


@session_start();

$user = $_SESSION['user']['name'] ?? null;
if (!$user)
{
	header('Location: /login.php?url='.urlencode($_SERVER['REQUEST_URI']??'/'));
	die();
}

