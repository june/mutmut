<?php

define('APPDIR', preg_replace('_lib$_', '', __DIR__));

require_once APPDIR . "lib/dates.php";


function now() {
	return Dates::Now();
}

function optkey($options)
{
	ksort($options);
	return '-'.http_build_query($options);
}

// Dates::SetNow(strtotime('2020-02-08'));
