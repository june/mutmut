<?php

require_once 'common.php';


class Dates
{
	const THREE_MONTHS = "3m";
	const SIX_MONTHS = "6m";
	const ONE_YEAR = "1y";
	const ALL = "all";
	
	public static $back_in_time = [
		self::THREE_MONTHS => ['dt' => '- 2 months',  'txt' => 'trois mois'],
		self::SIX_MONTHS =>   ['dt' => '- 5 months',  'txt' => 'six mois'],
		self::ONE_YEAR =>     ['dt' => '- 11 months', 'txt' => 'un an'],
		self::ALL =>          ['dt' => '×',           'txt' => 'tout'],
	];
	
	private static $now = null;
	public static function Now()
	{
		return self::$now ?: time();
	}
	
	public static function SetNow(int $timestamp)
	{
		self::$now = $timestamp;
	}
	
	private static function french_month($m) { return ['','janvier','février','mars','avril','mai','juin','juillet','août','septembre','octobre','novembre','décembre'][$m]; }
	
	public static function CurrMonth()
	{
		return self::Month(now());
	}
	
	public static function LastMonth()
	{
		return self::Month(self::PrevMonth(now()));
	}
	
	public static function ComingMonth()
	{
		return self::Month(self::NextMonth(now()));
	}
	
	public static function Month ($date)
	{
		if (is_string($date) && preg_match('/\d{4}-\d{2}/',$date))
			$date = strtotime("$date-01");
		
		$year = date('Y', $date);
		
		return self::french_month((int)date('m',$date))
			. ($year == date('Y', Dates::now()) ? "" : " $year");
	}
	
	public static function PrevMonth ($date)
	{
		if (is_int($date) || is_numeric($date))
			$date = date('Y-m-d',$date);
		return strtotime($date . " -1 month");
	}
	
	public static function NextMonth ($date)
	{
		if (is_int($date) || is_numeric($date))
			$date = date('Y-m-d',$date);
		return strtotime($date . " +1 month");
	}
	
	public static function FirstDayOfMonthMidnight($month) {
		switch ($month)
		{
			case 'next':
			case 'this':
			case 'last':
				$delta = [
				'next' => '+1 month',
				'this' => '',
				'last' => '-1 month',
				][$month];
				
				$month = date('Y-m', now());
				
				return strtotime("first day of $month $delta, midnight");
				break;
		}
		$ts = strtotime("first day of $month, midnight");
		if (!$ts)
			throw \Exception("Unable to compute strtotime('first day of $month, midnight')");
		return $ts;
	}
	
	public static function LastDayOfMonth235959($month) {
		switch ($month)
		{
			case 'next':
			case 'this':
			case 'last':
				$delta = [
					'next' => '+1 month',
					'this' => '',
					'last' => '-1 month',
				][$month];
				
				$month = date('Y-m', now());
				
				return strtotime("last day of $month $delta, 23:59:59");
				break;
		}
		$ts = strtotime("last day of $month, 23:59:59");
		if (!$ts)
			throw \Exception("Unable to compute strtotime('last day of $month, 23:59:59')");
		return $ts;
	}
	
	public static function FrenchDate (int $date)
	{
		return date("d/m/Y", $date);
	}
	
	public static function GetInterval($timestamp)
	{
		if ($timestamp == Dates::ALL)
			return [
				'thatmonth' => 0,
				'nextmonth' => strtotime('9999-99-99'),
			];
		
		if (isset(self::$back_in_time[$timestamp]))
			return [
				'thatmonth' => Dates::FirstDayOfMonthMidnight(date('Y-m', strtotime(date('Y-m-d', now()) . self::$back_in_time[$timestamp]['dt']))),
				'nextmonth' => now(),
			];
		
		return [
			'thatmonth' => strtotime(date('Y-m',$timestamp)),
			'nextmonth' => strtotime(date('Y-m',$timestamp)." +1 month"),
		];
	}
	
	public static function Antedated(int $timestamp)
	{
		if (!$timestamp)
			return false;
		
		$that_month = date('Y-m', $timestamp);
		
		return $timestamp == strtotime("last day of $that_month, 23:59:59");
	}
	
}

