<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require_once 'dates.php';


final class datesTest extends TestCase
{
	public function testNow()
	{
		Dates::SetNow(strtotime('2020-02-15 23:01:47'));
		$this->assertEquals(now(), strtotime('2020-02-15 23:01:47'));
	}
	
	// TODO: user data provider -- https://phpunit.readthedocs.io/en/9.5/writing-tests-for-phpunit.html#data-providers
	public function testFirstLastDay()
	{
		$this->assertEquals( Dates::FirstDayOfMonthMidnight('2021-02'), strtotime('2021-02-01 00:00:00') );
		$this->assertEquals(    Dates::LastDayOfMonth235959('2021-02'), strtotime('2021-02-28 23:59:59') );
		
		Dates::SetNow(strtotime('2020-01'));
		$this->assertEquals( Dates::FirstDayOfMonthMidnight('this'), strtotime('2020-01-01 00:00:00') );
		$this->assertEquals( Dates::FirstDayOfMonthMidnight('last'), strtotime('2019-12-01 00:00:00') );
		$this->assertEquals( Dates::FirstDayOfMonthMidnight('next'), strtotime('2020-02-01 00:00:00') );
		$this->assertEquals(    Dates::LastDayOfMonth235959('this'), strtotime('2020-01-31 23:59:59') );
		$this->assertEquals(    Dates::LastDayOfMonth235959('last'), strtotime('2019-12-31 23:59:59') );
		$this->assertEquals(    Dates::LastDayOfMonth235959('next'), strtotime('2020-02-29 23:59:59') );
	}
	
	public function testAntedated()
	{
		$this->assertEquals(Dates::Antedated(strtotime('2021-02-01 23:59:59')), false);
		$this->assertEquals(Dates::Antedated(strtotime('2021-02-28 13:54:22')), false);
		$this->assertEquals(Dates::Antedated(strtotime('2021-02-28 23:59:59')), true);
		$this->assertEquals(Dates::Antedated(strtotime('2020-02-28 23:59:59')), false);
		$this->assertEquals(Dates::Antedated(strtotime('2020-02-29 23:59:59')), true);
	}
	
	public function testMonthFunctions()
	{
		Dates::SetNow(strtotime('2020-02-17'));
		$this->assertEquals(Dates::LastMonth(),   'janvier');
		$this->assertEquals(Dates::CurrMonth(),   'février');
		$this->assertEquals(Dates::ComingMonth(), 'mars');
		
		$this->assertEquals(Dates::PrevMonth(strtotime('2020-01-07')), strtotime('2019-12-07'));
		$this->assertEquals(Dates::PrevMonth(          '2020-01-07'),  strtotime('2019-12-07'));
		$this->assertEquals(Dates::NextMonth(strtotime('2020-01-07')), strtotime('2020-02-07'));
		$this->assertEquals(Dates::NextMonth(          '2020-01-07'),  strtotime('2020-02-07'));
	}
	
	public function testFrenchDate()
	{
		$this->assertEquals(Dates::FrenchDate(strtotime('2020-01-25')), '25/01/2020');
	}
}
