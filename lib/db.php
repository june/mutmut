<?php


class DB
{
	private static $db = null;
	
	private static $SQL_UPGRADES = [
		'CREATE TABLE IF NOT EXISTS users (
			name     TEXT PRIMARY KEY,
			pronoun  TEXT,
			role     TEXT,  -- member/accountant
			password TEXT,
			since    INTEGER NOT NULL
			)',
		'CREATE TABLE IF NOT EXISTS contribute (
			date      INTEGER NOT NULL,
			amount    REAL    NOT NULL,
			user_from INTEGER NOT NULL
			)',
		'CREATE TABLE IF NOT EXISTS needs (
			date    INTEGER NOT NULL,
			amount  REAL    NOT NULL,
			user_to INTEGER NOT NULL
			)',
		'CREATE TABLE IF NOT EXISTS payments (
			why          TEXT    NOT NULL,  -- redistribution or need
			date         INTEGER NOT NULL,
			amount       REAL    NOT NULL,
			means        TEXT    NOT NULL,
			user_from    INTEGER NOT NULL,
			user_to      INTEGER NOT NULL,
			needid       INTEGER,
			confirm_date INTEGER            -- null means the payment hasnt been confirmed yet
			)',
		'fn:migration_add_admin',
		'fn:migration_users_until',
	];
	
	private static $testing = false;
	
	public static function SetTesting($bool)
	{
		self::$testing = $bool;
	}
	
	private static function ensuredb ()
	{
		if (is_null(self::$db))
		{
			$dbpath = APPDIR . '/data/mutmut.sqlite3';
			
			if (self::$testing)
				$dbpath = '/tmp/mutmut-'.date('Y-m-d-H:i:s').'.sqlite3';
			
			self::$db = new PDO('sqlite:' . $dbpath);
			self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
	}
	
	private static function updatedb ()
	{
		self::ensuredb();
		
		foreach (self::$SQL_UPGRADES as $create)
		{
			self::migrate($create);
		}
	}
	
	private static function migrate($migration)
	{
		if (preg_match('/^fn:(.*)$/', $migration, $m))
		{
			DB::{$m[1]}();
			return;
		}
		
		$res = self::_query($migration);
		if ($res === false)
			throw new \Exception(self::$db->errorInfo());
	}
	
	private static function migration_add_admin()
	{
		try {
			$users = self::_query('SELECT * FROM users');
			if (count($users) == 0)
			{
				$since = Dates::FirstDayOfMonthMidnight('this');
				$pwd = sha1('admin');
				self::_query("INSERT INTO users (name, password, role, since) VALUES ('admin', '$pwd', 'accountant', $since);");
			}
		}
		catch (\Exception $e)
		{}
	}
	
	private static function migration_users_until()
	{
		try {
			self::_query('ALTER TABLE users ADD COLUMN until INTEGER NULL');
		}
		catch (\Exception $e)
		{}
	}
	
	public static function GetOldestDataDate ()
	{
		$oldest_date = now();
		foreach (['contribute','payments','needs'] as $table)
		{
			$oldest = self::Query("SELECT MIN(date) AS oldest FROM $table")[0]['oldest'];
			if (!is_null($oldest) && $oldest < $oldest_date)
				$oldest_date = $oldest;
		}
		return strtotime(date('Y-m-01',$oldest_date));
	}
	
	public static function GetMonths ()
	{
		self::updatedb();
		
		$oldest_date = self::GetOldestDataDate();
		$date = now();
		$months = [];
		while ($date > $oldest_date)
		{
			$date = strtotime(date('Y-m-01 00:00:00',$date).'-1 month');
			$months[] = $date;
		}
		return $months;
	}
	
	public static function Query ($query, $values=[])
	{
		self::ensuredb();
		
		try {
			return self::_query($query,$values);
		}
		catch (\Exception $e) {
			self::updatedb();
			return self::_query($query,$values);
		}
	}
	
	public static $queries = [];
	private static function _query ($query, $values=[])
	{
		$q = $query . "\n?" . http_build_query($values);
		self::$queries[$q] = isset(self::$queries[$q]) ? self::$queries[$q]+1 : 1;
		
		$st = self::$db->prepare($query);
		
		foreach ($values as $k => $v)
			$st->bindValue($k, $v);
		
		$st->execute();
		$st->setFetchMode(PDO::FETCH_ASSOC);
		return $st->fetchAll();
	}
	
}
