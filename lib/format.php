<?php

require_once '../lib/dates.php';


class Format
{
	
	public static function Money ($amount,$nbdec)
	{
		if ($nbdec==0)
			return round($amount);
		$nbdec = $amount - (int)$amount == 0 ? 0 : 2;
		return number_format($amount,$nbdec,',',' ');
	}
	
	public static function MeansPayment ($means)
	{
		return [
			'check' => 'par chèque',
			'cash' => 'en espèces',
			'transfer' => 'par virement',
			'paypal' => 'via paypal',
		][$means] ?? $means;
	}
	
}
