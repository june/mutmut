<?php

require_once APPDIR . 'lib/db.php';
require_once APPDIR . 'lib/users.php';
require_once APPDIR . 'lib/dates.php';


class Money
{
	const CONTRIB_NONE = 1;
	const CONTRIB_EDITABLE = 2;
	const CONTRIB_NEXTMONTH = 3;
	const CONTRIB_NOTUSERYET = 4;
	
	private static $cache;
	
	public static function DeclareContribution ($username, $contribution)
	{
		self::$cache = [];
		
		@session_start();
		$editable = self::ContributionEditable($username);
		$contrib_time = now();
		switch ($editable) {
			case self::CONTRIB_NONE:
			case self::CONTRIB_EDITABLE:
				// cases are okay and variable `$contrib_time = now()` is correct
				break;
			case self::CONTRIB_NEXTMONTH:
				$contrib_time = Dates::FirstDayOfMonthMidnight('next');
				break;
			default:
				throw new \Exception("Can't declare or edit declaration");
		}
		
		$delete_after = strtotime(date('Y-m-01',$contrib_time));
		
		$userid = Users::GetID($username);
		DB::Query(
			'DELETE FROM contribute WHERE date >= :thismonth AND user_from = :user_from',
			[
				':thismonth' => $delete_after,
				':user_from' => $userid,
			]
		);
		DB::Query(
			'INSERT INTO contribute (date, amount, user_from) VALUES (:d, :a, :f)',
			[
				':d' => $contrib_time,
				':a' => round($contribution),
				':f' => $userid,
			]
		);
	}
	
	public static function GetContributions ($options=[])
	{
		$ckey = __FUNCTION__ . optkey($options);
		if (isset(self::$cache[$ckey]))
			return self::$cache[$ckey];
		
		$options = array_merge(['time' => now()], $options);
		$i = Dates::GetInterval($options['time']);
		
		$query = '
			SELECT u.rowid AS userid, name, amount, date
			FROM contribute c
			JOIN users u ON u.rowid = c.user_from
			WHERE date >= :thatmonth
				AND date <  :nextmonth
			';
		$values = [
			':thatmonth' => $i['thatmonth'],
			':nextmonth' => $i['nextmonth'],
		];
		
		if ($options['username'] ?? false)
			$options['userid'] = Users::GetID($options['username']);
		
		if ($options['userid'] ?? false)
		{
			$query .= ' AND user_from = :user_from ';
			$values[':user_from'] = $options['userid'];
		}
		
		$query .= 'ORDER BY date DESC';
		
		return self::$cache[$ckey] = array_map(
			fn($c) => array_merge($c,[
			'editable' => $c['date'] >= strtotime(date('Y-m-01',now())) && (int)date('d',now()) <= 5
			|| $c['date'] >= Dates::FirstDayOfMonthMidnight('next'),
			]),
			DB::Query($query,$values));
	}
	
	public static function GetContributionsTotals ($options=[])
	{
		$contrib = self::GetContributions($options);
		$sum_contrib = array_sum(array_map(fn($c) =>$c['amount'],$contrib));
		$avg_contrib = $sum_contrib / count(Users::GetAll(['time'=>$options['time']]));
		
		return [
			'sum_contrib' => $sum_contrib,
			'avg_contrib' => $avg_contrib,
			'sum_contrib_redistr' => $sum_contrib/2,
			'avg_contrib_redistr' => $avg_contrib/2,
		];
	}
	
	public static function ContributionEditable ($username)
	{
		$user = Users::GetByName($username);
		if ($user['since'] > now())
			return self::CONTRIB_NOTUSERYET;
		
		$contribution = self::GetContributions(['username'=>$username]);
		
		if (count($contribution) == 0)
			return self::CONTRIB_NONE;
		
		if ($contribution[0]['editable'])
			return self::CONTRIB_EDITABLE;
		
		if ((int)date('d',now()) >= 25)
			return self::CONTRIB_NEXTMONTH;
		
		return 0;
	}
	
	public static function WhoDidntDeclare ()
	{
		$name = fn($r) => $r['name'];
		$users = array_map($name, Users::GetAll());
		$declared = array_map($name, self::GetContributions());
		
		return array_diff($users,$declared);
	}
	
	public static function GetNeeds ($options=[])
	{
		$options = array_merge(['time' => now()], $options);
		
		$i = Dates::GetInterval($options['time']);
		
		$needs = DB::Query('
			SELECT n.rowid as needid, user_to, name, date, amount,
				(SELECT SUM(amount) FROM payments p WHERE p.needid = n.rowid) AS paid,
				(SELECT COUNT(*)    FROM payments p WHERE p.needid = n.rowid) AS nb_payments,
				(SELECT COUNT(*)    FROM payments p WHERE p.needid = n.rowid AND p.confirm_date IS NOT NULL) AS nb_confirmed_payments
			FROM needs n
			JOIN users u ON u.rowid = n.user_to
			WHERE date >= :thatmonth
			  AND date <  :nextmonth
			  OR  paid IS NULL OR paid != amount
			  ',
			[
				':thatmonth' => $i['thatmonth'],
				':nextmonth' => $i['nextmonth'],
			]
		);
		
		foreach ($needs as &$n)
		{
			$n['payments'] = Money::GetPayments(['reason'=>'need','needid'=>$n['needid'],'time'=>Dates::ALL]);
			$total_amount = array_sum(array_map(fn($p) => $p['amount'],$n['payments']));
			$n['fulfilled'] = $total_amount == $n['amount'];
			$n['confirmed'] = $total_amount == $n['amount'] && $n['nb_payments'] == $n['nb_confirmed_payments'];
		}
		
		return $needs;
	}
	
	public static function AddNeed ($username,$amount)
	{
		self::$cache = [];
		
		DB::Query(
			'INSERT INTO needs (date, amount, user_to) VALUES (:d, :a, :u)',
			[
				':d' => now(),
				':a' => $amount,
				':u' => Users::GetID($username),
			]
		);
	}
	
	public static function EditNeed ($needid, $amount)
	{
		self::$cache = [];
		
		// TODO: check that needs exists and is allowed to be modified (= no payments attached)
		
		if ($amount !== '0' && (int)$amount == 0)  // contribution must be an integer; 0 is also valid
			die('Montant incorrect');
		
		$amount = (int)$amount;
		
		if ($amount > 0)
			DB::Query('UPDATE needs SET amount = :a WHERE rowid = :id', [':a' => $amount, ':id' => $needid]);
		else
			DB::Query('DELETE FROM needs WHERE rowid = :id', [':id' => $needid]);
	}
	
	public static function CreatePayment (int $user_from, int $user_to, int $amount, string $means, string $reason, int $needid=null)
	{
		self::$cache = [];
		
		// consistency checks
		$users = Users::GetAll();
		foreach ([$user_from,$user_to] as $userid)
			if (count(array_filter($users, fn($u) => $u['rowid'] == $userid)) == 0)
				throw new \Exception("Unknown userID $userid");
		
		if (!in_array($means,['cash','check','transfer','paypal']))
			throw new \Exception("Unknown means of payment '$means'");
		
		if (!in_array($reason,['redistribution','need']))
			throw new \Exception("Unknown reason for payment '$reason'");
		
		if ((int)$amount == 0)
			throw new \Exception("Invalid amount '$amount'");
		
		$time = now();
		
		/* Special case: if redistributions haven't started this month, write a need's payment in last month's accountability.
		 * This way, the need is taken into account before redistribution, and re-balanced.
		 */
		if ($reason == 'need' && count(self::GetPayments(['reason'=>'redistribution'])) == 0)
			$time = Dates::LastDayOfMonth235959('last');
		
		DB::Query(
			'INSERT INTO payments (date, amount, means, user_from, user_to, confirm_date, why, needid) VALUES (:d, :a, :m, :f, :t, :c, :w, :n)',
			[
				':d' => $time,
				':a' => $amount,
				':m' => $means,
				':f' => $user_from,
				':t' => $user_to,
				':c' => null,  // not confirmed, just created
				':w' => $reason,
				':n' => $needid,
			]);
	}
	
	public static function DeletePayment ($paymentid)
	{
		self::$cache = [];
		DB::Query('DELETE FROM payments WHERE rowid = :id', [':id' => (int)$paymentid]);
	}
	
	public static function EditPayment ($paymentid, $amount)
	{
		self::$cache = [];
		
		if ((int)$amount == 0)
			throw new \Exception("Invalid amount '$amount'");
		
		DB::Query('UPDATE payments SET amount = :a WHERE rowid = :id', [
			':a'  => $amount,
			':id' => (int)$paymentid,
		]);
	}
	
	public static function ConfirmPayment ($paymentid, $means)
	{
		self::$cache = [];
		
		DB::Query('
			UPDATE payments
			SET confirm_date = :time,
				means = :means
			WHERE rowid = :id
			',
			[
				':time' => now(),
				':means' => $means,
				':id' => $paymentid,
			]);
	}
	
	public static function GetPayments($options)
	{
		$ckey = __FUNCTION__ . optkey($options);
		if (isset(self::$cache[$ckey]))
			return self::$cache[$ckey];
		
		$options = array_merge(['time' => now()], $options);
		$i = Dates::GetInterval($options['time']);
		
		$reason = $options['reason'] ?? null;
		if (!in_array($reason,['redistribution','need','all']))
			throw new \Exception('Reason not defined or unknown');
		
		$query = '
			SELECT p.rowid,
				uf.name AS name_from, ut.name AS name_to, uf.rowid AS id_from, ut.rowid AS id_to,
				p.date, p.amount, IIF(p.user_from = p.user_to, "retrait", means) AS means, confirm_date, needid,
				n.date AS needdate
			FROM payments p
			JOIN users uf ON uf.rowid = p.user_from
			JOIN users ut ON ut.rowid = p.user_to
			LEFT JOIN needs n ON n.rowid = p.needid
			WHERE p.date >= :thatmonth
			  AND p.date <  :nextmonth
	  ';
		$values = [
			':thatmonth' => $i['thatmonth'],
			':nextmonth' => $i['nextmonth'],
		];
		
		if ($reason != 'all'){
			$query .= ' AND why = :why ';
			$values[':why'] = $reason;
		}
		
		if ($options['needid'] ?? false)
		{
			$query .= ' AND needid = :needid ';
			$values[':needid'] = $options['needid'];
		}
		
		if ($options['for_or_to'] ?? false)
		{
			$query .= ' AND (p.user_to = :user_to OR p.user_from = :user_from) ';
			$values[':user_to'] = Users::GetID($options['for_or_to']);
			$values[':user_from'] = Users::GetID($options['for_or_to']);
		}
		
		if (isset($options['confirmed']))
			$query .= ' AND confirm_date ' . ($options['confirmed'] ? 'IS NOT NULL' : 'IS NULL');
		
		return self::$cache[$ckey] = DB::Query($query,$values);
	}
	
	public static function GetEquilibrium ($options=[])
	{
		$options = array_merge(['time' => now()], $options);
		
		$users = Users::GetAll(['time'=>$options['time']]);
		if (empty($users))
			return [];
		
		$contrib = Money::GetContributions($options);
		$redistr_payments = Money::GetPayments(array_merge($options,['reason'=>'redistribution']));
		$needs_payments = Money::GetPayments(array_merge($options,['reason'=>'need']));
		$contrib_totals = self::GetContributionsTotals($options);
		
		if (count($contrib) < count($users))
			return [];
		
		foreach ($users as &$user)
		{
			$user['contrib'] = array_filter($contrib, fn($c) => $c['userid'] == $user['rowid']);
			$user['contrib'] = count($user['contrib']) > 0 ? round(array_values($user['contrib'])[0]['amount']) : 0;
			$user['diff_avg_contrib'] = max(0, $contrib_totals['avg_contrib'] - $user['contrib']);
		}
		unset($user);
		
		$total_diff_contrib = array_sum(array_map(fn($u) => $u['diff_avg_contrib'], $users));
		
		$prev_month = strtotime(date('Y-m-01',$options['time']) . '-1 month');
		$prev_registers = self::GetRegisters(['time' => $prev_month]);
		$prev_register_avg = array_sum(array_map(fn($r) => $r['register'], $prev_registers)) / count($users);
		
		$equil = [];
		foreach ($users as $user)
		{
			$userid = $user['rowid'];
			
			$ratio_diff_contrib = $total_diff_contrib ? $user['diff_avg_contrib'] / $total_diff_contrib : 0;
			
			$payments = [
				'pay_redistr' => array_filter($redistr_payments, fn($p) => $p['id_from'] == $userid),
				'pay_needs'   => array_filter($needs_payments,   fn($p) => $p['id_from'] == $userid),
				'receive'     => array_filter($redistr_payments, fn($p) => $p['id_to']   == $userid),
			];
			foreach ($payments as $op => &$pp)
				$pp = array_map(fn($p) => $p['amount'], $pp);
			unset($pp);
			
			$user_redistr = round($contrib_totals['sum_contrib_redistr'] * $ratio_diff_contrib);
			
			$prev_register = array_filter($prev_registers, fn($c) => $c['userid'] == $userid);
			$prev_register = empty($prev_register) ? 0 : array_values($prev_register)[0]['register'];
			$diff_register = $prev_register_avg - $prev_register;
			
			$user_needs = $user_redistr - $user['contrib'] + $contrib_totals['avg_contrib_redistr'] + $diff_register;
			$user_equil = -$user_needs - array_sum($payments['pay_redistr']) + array_sum($payments['receive']);
			
			$user_register_no_needs = $prev_register + $user['contrib'] - $user_redistr - array_sum($payments['pay_redistr']) + array_sum($payments['receive']);
			$user_register = $user_register_no_needs - array_sum($payments['pay_needs']);
			
			$equil[$userid] = [
				'name' => $user['name'],
				'needs' => $user_needs,
				'equil' => $user_equil,
				'contrib' => $user['contrib'],
				'diff_avg_contrib' => $user['diff_avg_contrib'],
				'ratio_diff_contrib' => $ratio_diff_contrib,
				'redistr' => $user_redistr,
				'prev_register' => $prev_register,
				'diff_register' => $diff_register,
				'payments' => $payments,
				'register' => $user_register,
				'register_no_needs' => $user_register_no_needs,
				
				// data non dependent on user
				'total_diff_contrib' => $total_diff_contrib,
				'prev_register_avg' => $prev_register_avg,
			];
		}
		
		return $equil;
	}
	
	// TODO: replace by redistribution payments freeze, decided by accountants
	public static function GetEquilibriumLeftToPay ($options=[])
	{
		$options = array_merge(['time' => now()], $options);
		$equil = self::GetEquilibrium($options);
		return array_sum(array_map(fn($e) => $e['equil'] > 0 ? $e['equil'] : 0, $equil));
	}
	
	public static function GetRegisters ($options=[])
	{
		$options = array_merge(['time' => now()], $options);
		
		return array_map(fn ($u) => [
			'userid' => $u['rowid'],
			'name' => $u['name'],
			'register' => self::GetRegister(array_merge($options,['username'=>$u['name']]))
		], Users::GetAll(['time'=>$options['time']]));
	}
	
	public static function GetRegister ($options=[])
	{
		$ckey = __FUNCTION__ . optkey($options);
		if (isset(self::$cache[$ckey]))
			return self::$cache[$ckey];
		
		$options = array_merge(['time' => now(), 'username' => null], $options);
		
		$equil = self::GetEquilibrium(['time'=>$options['time']]);
		if (empty($equil))  // e.g. not all contribs declared yet this month
			$equil = self::GetEquilibrium(['time'=>Dates::PrevMonth($options['time'])]);
		
		if (!is_null($options['username']))
			$equil = array_filter($equil, fn($e) => $e['name'] == $options['username']);
		
		return self::$cache[$ckey] = array_sum(array_map(fn($e) => $e['register'], $equil));
	}
	
	private static function payment_csv_line($out,$users,$payment)
	{
		$from_index = array_keys(array_filter($users, fn($u) => $u['name'] == $payment['name_from']))[0];
		$to_index   = array_keys(array_filter($users, fn($u) => $u['name'] == $payment['name_to'])  )[0];
		$line = array_fill(0,count($users),'');
		$line[$to_index]   = +$payment['amount'];
		$line[$from_index] = -$payment['amount'];
		$label = 'Équilibrage';
		if ($payment['needid'])
			$label = $from_index == $to_index ? 'Retrait' : 'Besoin';
		fputcsv($out,array_merge([$label], $line, [$label == "Besoin" ? "pour {$payment['name_to']}" : '']));
	}
	
	public static function Export ($month)
	{
		$time = strtotime("$month-01");
		
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=$month.csv");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		
		$out = fopen('php://output','w');
		
		// users
		$users = Users::GetAll(['time'=>$time]);
		fputcsv($out,array_merge(['Membre'],array_map(fn($u) => $u['name'], $users),['Total']));
		
		// prev month registers
		$registers = array_map(fn($r) => $r['register'], self::GetRegisters(['time'=>Dates::PrevMonth($time)]));
		fputcsv($out,array_merge(['Report mois précédent'],$registers,[array_sum($registers)]));
		
		// redistributions
		$equil = self::GetEquilibrium(['time'=>$time]);
		$redistr = array_map(fn($e) => $e['redistr'] ? -$e['redistr'] : 0, $equil);
		fputcsv($out,array_merge(['Redistribution'], $redistr, [-array_sum($redistr)]));
		
		// contributions
		$contribs = self::GetContributions(['time'=>$time]);
		$contribs = array_map(fn($c) => $c['amount'], $contribs);
		fputcsv($out,array_merge(['Cotisation'], $contribs, [array_sum($contribs)]));
		
		// redistributions
		$redistr = self::GetPayments(['time'=>$time, 'reason'=>'redistribution']);
		foreach ($redistr as $payment)
			self::payment_csv_line($out,$users,$payment);
		
		$registers_no_needs = array_map(fn($e) => $e['register_no_needs'], $equil);
		fputcsv($out,array_merge(['Caisses après redistribution'],$registers_no_needs,[array_sum($registers_no_needs)]));
		
		$needs = self::GetPayments(['time'=>$time, 'reason'=>'need']);
		foreach ($needs as $payment)
			self::payment_csv_line($out,$users,$payment);
		
		// month end registers
		$registers = array_map(fn($r) => $r['register'], self::GetRegisters(['time'=>$time]));
		fputcsv($out,array_merge(['Caisses fin de mois'],$registers,[array_sum($registers)]));
		
		fclose($out);
		die();
	}
	
	public static function HandlePaymentAction ($redirect_to)
	{
		$action = $_POST['action'] ?? null;
		switch ($action)
		{
			case 'ask':
				@session_start();
				Money::AddNeed($_SESSION['user']['name'], $_POST['amount']);
				header("Location: $redirect_to");
				die();
				break;
				
			case 'add':
				Money::CreatePayment($_POST['user_from'], $_POST['user_to'], $_POST['amount'], $_POST['means'], $_POST['reason'], $_POST['needid']?:null);
				header("Location: $redirect_to");
				die();
				break;
				
			case 'confirm':
				Money::ConfirmPayment($_POST['paymentid'], $_POST['means']);
				header("Location: $redirect_to");
				die();
				break;
				
			case 'delete':
				Money::DeletePayment($_POST['paymentid']);
				header("Location: $redirect_to");
				die();
				break;
				
			case 'edit':
				Money::EditPayment($_POST['paymentid'], $_POST['amount']);
				header("Location: $redirect_to");
				die();
				break;
		}
	}
	
	public static function HandleNeedAction ($redirect_to)
	{
		$action = $_POST['action'] ?? null;
		switch ($action)
		{
			case 'edit_need':
				Money::EditNeed($_POST['needid'], $_POST['amount']);
				header("Location: $redirect_to");
				die();
				break;
		}
	}
}
