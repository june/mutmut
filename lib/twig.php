<?php

require_once '/usr/share/php/Twig/autoload.php';
require_once '../lib/users.php';
require_once '../lib/format.php';
require_once '../lib/money.php';
require_once '../lib/dates.php';


class Twig
{
	private static $page_month = null;
	private static $recent = null;
	private static $data = [];
	
	public static function Display ($tpl, $data=[], $extra_data=[])
	{
		$t1 = microtime(true);
		
		self::load_data($data);
		self::$data = array_merge(self::$data,$extra_data);
		
		$loader = new \Twig\Loader\FilesystemLoader(__DIR__.'/../twig');
		
		$opt = ['cache' => __DIR__.'/../twig/cache'];
		if (getenv('DEV'))
			unset($opt['cache']);
		
		$twig = new \Twig\Environment($loader,$opt);
		
		$twig->addFilter(new \Twig\TwigFilter('yyyy_mm',     function ($ts)  { return date('Y-m',$ts); }));
		$twig->addFilter(new \Twig\TwigFilter('human_month', function ($ts)  { return Dates::Month($ts); }));
		$twig->addFilter(new \Twig\TwigFilter('human_date',  function ($ts)  { return Dates::FrenchDate($ts); }));
		$twig->addFilter(new \Twig\TwigFilter('minus1month', function ($ts)  { return Dates::PrevMonth($ts); }));
		$twig->addFilter(new \Twig\TwigFilter('money',       function ($sum) { return Format::Money($sum,0); }));
		$twig->addFilter(new \Twig\TwigFilter('money2',      function ($sum) { return Format::Money($sum,2); }));
		$twig->addFilter(new \Twig\TwigFilter('money_abs',   function ($sum) { return Format::Money(abs($sum),0); }));
		$twig->addFilter(new \Twig\TwigFilter('pay_means',   function ($m)   { return Format::MeansPayment($m); }));
		$twig->addFilter(new \Twig\TwigFilter('count',       function ($a)   { return count($a); }));
		$twig->addFilter(new \Twig\TwigFilter('month_users', function ($ts)  { return Users::GetAll(['since'=>$ts]); }));
		
		self::$data['now'] = Dates::Now();
		self::$data['time'] = time();
		
		$t2 = microtime(true);
		
		echo $twig->render($tpl, self::$data);
		
		if (getenv('DEV'))
		{
			echo "<pre class='debug'>";
			echo round(($t2-$t1)*1000) . " ms\n";
			echo array_sum(DB::$queries) . " queries\n";
			arsort(DB::$queries);
			print_r(array_flip(DB::$queries));
			echo "</pre>";
		}
	}
	
	private static function url ($params) {
		$get = array_merge($_GET, $params);
		return self::$data['curr_url'] . '?' . http_build_query($get);
	}
	
	private static function load_data ($data=[])
	{
		@session_start();
		
		self::$page_month = isset($_GET['t']) ? (int)$_GET['t'] : now();
		self::$recent = $_GET['r'] ?? Dates::THREE_MONTHS;
		
		self::$data = [
			'user' => $_SESSION['user'] ?? null,
			'menu' => [
				'/' => 'Activité',
				'/contributions.php' => 'Cotisations',
				'/redistribution.php' => 'Redistribution',
				'/needs.php' => 'Besoins',
				'/payments.php' => 'Paiements',
				'/about.php' => '?',
			],
			'curr_url' => explode('?',$_SERVER['REQUEST_URI'])[0],
			'redirect_url' => $_GET['url'] ?? '',
		];
		
		self::$data['curr_month'] = Dates::CurrMonth();
		self::$data['next_month'] = Dates::ComingMonth();
		self::$data['last_month'] = Dates::LastMonth();
		self::$data['page_month'] = Dates::Month(self::$page_month);
		self::$data['past_months'] = DB::GetMonths();
		
		self::$data['page_month_warn'] = isset($_GET['t']) ? '['.self::$data['page_month'].']' : '';
		
		self::$data['users'] = Users::GetAll(['time'=>self::$page_month]);
		self::$data['nb_users'] = count(self::$data['users']);
		
		$equil = Money::GetEquilibriumLeftToPay(['time'=>self::$page_month]);
		self::$data['warning_redistribution_left'] = $equil > self::$data['nb_users'] * 3;  // rule of thumb equilibrium
		
		self::$data['recent'] = self::$recent;
		self::$data['urls_recent'] = [];
		foreach (Dates::$back_in_time as $r => $recent)
			self::$data['urls_recent'][$r] = array_merge($recent, ['url' => self::url(['r' => $r])]);
		
		foreach ($data as $key)
			self::{"load_$key"}();
	}
	
	private static function load_post ()
	{
		self::$data['post'] = $_POST;
	}
	
	private static function load_get ()
	{
		self::$data['get'] = $_GET;
	}
	
	private static function load_registers ()
	{
		self::$data['global_register'] = Money::GetRegister();
		self::$data['user_registers'] = Money::GetRegisters();
	}
	
	private static function load_contributions ()
	{
		self::$data['user_contributions'] = Money::GetContributions(['username'=>$_SESSION['user']['name'], 'time'=>self::$recent]);
		
		// this month or next month's contribution for logged in user
		$editable = Money::ContributionEditable($_SESSION['user']['name']);
		
		$next_month_contrib = Money::GetContributions([
			'username' => $_SESSION['user']['name'],
			'time' => Dates::FirstDayOfMonthMidnight('next')
		])[0] ?? '';
		
		self::$data['user_contrib'] = [
			'show_link' => $editable == Money::CONTRIB_NONE || $editable == Money::CONTRIB_NEXTMONTH && !$next_month_contrib,
			'for_month' => $editable == Money::CONTRIB_NEXTMONTH ? self::$data['next_month'] : self::$data['curr_month'],
			'edit_contrib' => $editable == Money::CONTRIB_NEXTMONTH
				? $next_month_contrib
				: Money::GetContributions(['username' => $_SESSION['user']['name']])[0] ?? '',
		];
		
		// everyone's contribs this month
		self::$data['month_contributions'] = Money::GetContributions(['time'=>self::$page_month]);
		usort(self::$data['month_contributions'], fn($c1,$c2) => mb_strtolower($c1['name']) > mb_strtolower($c2['name']));
		self::$data['month_contributions_missing'] = array_diff(
			array_map(fn($u) => $u['name'], self::$data['users']),
			array_map(fn($c) => $c['name'], self::$data['month_contributions'])
			);
		self::$data = array_merge(self::$data, Money::GetContributionsTotals(['time'=>self::$page_month]));
	}
	
	private static function load_needs ()
	{
		self::$data['needs'] = Money::GetNeeds(['time'=>self::$page_month]);
		usort(self::$data['needs'], fn($n1,$n2)=>$n1['date']<$n2['date']);
	}
	
	private static function load_equilibrium ()
	{
		self::$data['equilibrium'] = Money::GetEquilibrium(['time'=>self::$page_month]);
		self::$data['equilibrium_left_to_pay'] = Money::GetEquilibriumLeftToPay(['time'=>self::$page_month]);
	}
	
	private static function load_payments_redistribution ()
	{
		self::$data['payments_redistribution'] = Money::GetPayments(['time'=>self::$page_month,'reason'=>'redistribution']);
	}
	
	private static function load_payments_mine ()
	{
		self::$data['payments_mine'] = Money::GetPayments([
			'reason'      => 'all',
			'time'        => Dates::ALL,
			'for_or_to'   => $_SESSION['user']['name'],
			'confirmed'   => false,
		]);
		usort(self::$data['payments_mine'], fn($p1,$p2) => $p1['name_from'] > $p2['name_from']);
	}
	
	private static function load_payments_others ()
	{
		self::load_payments_mine();
		
		self::$data['payments_others'] = array_udiff(
			Money::GetPayments(['reason'=>'all']),
			self::$data['payments_mine'],
			fn($p1,$p2) => ($p1['rowid'] == $p2['rowid']) ? 0 : -1
		);
		usort(self::$data['payments_others'], fn($p1,$p2) => $p1['name_from'] > $p2['name_from']);
	}
	
	private static function load_transactions ()
	{
		$contribs = Money::GetContributions(['time'=>self::$recent]);
		$payments = Money::GetPayments(['time'=>self::$recent,'reason'=>'all']);
		
		$transactions = [];
		
		foreach ($contribs as $c)
			$transactions[date('Y-m',$c['date'])]['transactions'][] = [
				'date' => $c['date'],
				'sign' => '+',
				'msgtype' => 'contribute',
				'data' => $c,
			];
		
		foreach ($payments as $p)
			$transactions[date('Y-m',$p['date'])]['transactions'][] = [
				'date' => $p['date'],
				'sign' => '-',
				'msgtype' => 'pay',
				'data' => $p,
				'antedated' => Dates::Antedated($p['date']),
			];
		
		$nextmonth = date('Y-m',Dates::FirstDayOfMonthMidnight('next'));
		$transactions[$nextmonth] = [
			'register' => Money::GetRegister(['time'=>strtotime("$nextmonth-01")]),
			'transactions' => [],
		];
		
		$thismonth = date('Y-m',now());
		if (!isset($transactions[$thismonth]))
			$transactions[$thismonth] = [
				'register' => Money::GetRegister(['time'=>strtotime("$thismonth-01")]),
				'transactions' => [],
			];
		
		foreach ($transactions as $month => &$t)
		{
			$time = strtotime("$month-01 -1month");
			$t['register'] = Money::GetRegister(['time'=>$time]);
			
			usort($t['transactions'], fn($t1,$t2) => $t1['date'] < $t2['date']);
		}
		uksort($transactions, fn($t1,$t2) => $t1 < $t2);
		
		self::$data['transactions'] = $transactions;
	}
	
}
