<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../lib/common.php';
require_once APPDIR . 'lib/users.php';
require_once APPDIR . 'lib/money.php';


final class GlobalTest extends TestCase
{
	protected function setUp(): void
	{
		DB::SetTesting(true);
	}
	
// 	protected function tearDown(): void
// 	{
// 		// TODO: remove db file
// 	}
	
	public function testAll()
	{
		Dates::SetNow(strtotime('2020-02-13'));
		
		// admin user in a newly created database
		$this->assertEquals(count(array_filter(Users::GetAll(), fn($u) => $u['name'] == 'admin')), 1);
		
		// create user
		Users::Create('bob', '', 'passbob1');
		$this->assertEquals(count(Users::GetAll()), 2);
		
		$a = Users::GetID('admin');
		$b = Users::GetID('bob');
		
		// declare contributions
		Dates::SetNow(strtotime('2020-02-14')); Money::DeclareContribution('admin', 150);
		Dates::SetNow(strtotime('2020-02-15')); Money::DeclareContribution('bob', 50);
		
		// first redistribution
		$equil = Money::GetEquilibrium();
		$this->assertEquals($equil[1]['needs'], -100);
		$this->assertEquals($equil[2]['needs'], +100);
		
		// balance money
		Money::CreatePayment($a, $b, 100, 'cash', 'redistribution');
		
		$equil = Money::GetEquilibrium();
		$this->assertEquals($equil[1]['equil'], 0);
		$this->assertEquals($equil[2]['equil'], 0);
		
		$registers = Money::GetRegisters();
		$this->assertEquals($registers[0]['register'], 50);
		$this->assertEquals($registers[1]['register'], 50);
		
		// someone comes in, they should be set to join next 1st of month
		Users::Create('camelia', '', 'magnolia78');
		
		$camelia = Users::GetbyName('camelia');
		$this->assertEquals($camelia['since'], strtotime('2020-03-01'));  // redistribution has happened; joins only next month
		
		$c = Users::GetID('camelia');
		
		Dates::SetNow(strtotime('2020-03-02')); Money::DeclareContribution('admin', 100);
		Dates::SetNow(strtotime('2020-03-05')); Money::DeclareContribution('bob', 100);
		Dates::SetNow(strtotime('2020-03-06')); Money::DeclareContribution('camelia', 200);
		
		// balance money, 2nd redistribution
		Money::CreatePayment($c, $a, 50, 'cash', 'redistribution');
		Money::CreatePayment($c, $b, 50, 'cash', 'redistribution');
		
		$equil = Money::GetEquilibrium();
		$this->assertEquals($equil[1]['equil'], 0);
		$this->assertEquals($equil[2]['equil'], 0);
		$this->assertEquals($equil[3]['equil'], 0);
		
		$registers = Money::GetRegisters();
		$this->assertEquals($registers[0]['register'], 100);
		$this->assertEquals($registers[1]['register'], 100);
		$this->assertEquals($registers[2]['register'], 100);
	}	
}
